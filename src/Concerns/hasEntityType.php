<?php
namespace KDA\Filament\Entities\Concerns;


trait hasEntityType{

    protected Closure | string | null $entityType = null;

    public function entityType(Closure | string $type): static
    {
        $this->entityType = $type;

        return $this;
    }

    public function getEntityType(): ?string
    {
        return $this->evaluate($this->entityType);
    }

}