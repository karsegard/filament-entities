<?php

namespace KDA\Filament\Entities;

use Filament\PluginServiceProvider;
use KDA\Filament\Entities\Livewire\EntityPicker;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
        //    'my-package-styles' => __DIR__ . '/../dist/app.css',
        'filament-entities' => __DIR__.'/../assets/css/entities.css',

    ];

    protected array $widgets = [
        //    CustomWidget::class,
    ];

    protected array $pages = [
        //    CustomPage::class,
    ];

    protected array $resources = [
        //     CustomResource::class,
    ];

    protected array $relationManagers = [
    ];

    public function configurePackage(Package $package): void
    {
        $package->name('filament-entities');
    }

    public function packageBooted(): void
    {
        parent::packageBooted();
        Livewire::component(EntityPicker::getName(), EntityPicker::class);
    }
}
