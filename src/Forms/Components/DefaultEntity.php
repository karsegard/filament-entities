<?php

namespace KDA\Filament\Entities\Forms\Components;

class DefaultEntity extends Entity
{
    protected string $view = 'filament-entities::entities.default';
}
