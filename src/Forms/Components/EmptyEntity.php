<?php

namespace KDA\Filament\Entities\Forms\Components;

class EmptyEntity extends Entity
{
    protected string $view = 'filament-entities::entities.empty';
}
