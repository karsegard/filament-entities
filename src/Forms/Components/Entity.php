<?php

namespace KDA\Filament\Entities\Forms\Components;

use Filament\Forms\Components\ViewField;

abstract class Entity extends ViewField
{
    public static function make(string $name = 'entity'): static
    {
        return parent::make($name);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->label('');
    }
}
