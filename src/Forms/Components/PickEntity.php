<?php

namespace KDA\Filament\Entities\Forms\Components;

use Closure;
use Filament\Forms\Components\Concerns;
use Filament\Forms\Components\Field;
use Filament\Forms\Components\Hidden;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use KDA\Filament\Entities\Concerns\hasEntityType;
use KDA\Filament\Entities\Livewire\EntityPicker;

class PickEntity extends Field
{
    use Concerns\HasHelperText;
    use Concerns\HasHint;
    use hasEntityType;
    use Concerns\CanBeValidated {
        required as traitRequired;
    }
    use Concerns\HasName;

    protected string $view = 'filament-entities::forms.components.pick-entity';

    protected string $tableComponent;

    protected Closure $renderRecordUsing;

   /* protected Closure | string | null $entityType = null;

    public function entityType(Closure | string $type): static
    {
        $this->entityType = $type;

        return $this;
    }

    public function getEntityType(): ?string
    {
        return $this->evaluate($this->entityType);
    }*/

    public function tableComponent($string): static
    {
        $this->tableComponent = $string;

        return $this;
    }

    public function renderRecordUsing(Closure $component): static
    {
        $this->renderRecordUsing = $component;

        return $this;
    }

    public function getRenderRecord()
    {
        $state = $this->getState();
        $record = null;

        if (isset($state['id']) && isset($state['type'])) {
            $record = $this->getRelatedModel($state['type'], $state['id']);
        }

        return $this->evaluate($this->renderRecordUsing, ['record' => $record]);
    }

    public function getTableComponent(): string
    {
        return this->tableComponent;
    }

    public function getTableComponentName(): string
    {
        return $this->tableComponent::getName();
    }

    public function getChildComponents(): array
    {
        return [
            /*   Hidden::make($typeColumn)->required($this->isRequired()),
            Hidden::make($keyColumn)->required($this->isRequired()),
*/
            $this->getRenderRecord(),
        ];
    }

    public function getRelationship(): MorphTo
    {
        return $this->getModelInstance()->{$this->getName()}();
    }

    public function getRelatedModel($type, $id): ?Model
    {
        if (! blank($type)) {
            return $type::find($id);
        }

        return null;
    }

    public function required(Closure|bool $condition = true): static
    {
        $this->traitRequired($condition);
        if ($condition === true) {
            $this->rules([
                function () {
                    return function (string $attribute, $value, Closure $fail) {
                        if (empty($value['id']) || empty($value['type'])) {
                            $fail("The {$attribute} is required.");
                        }
                    };
                },
            ]);
        }

        return $this;
    }

    public function isEmpty()
    {
        $state = $this->getState();

        return   empty($state['id']) || empty($state['type']);
    }

    public static function defaultState($model): array
    {
        if ($model) {
            return [
                'id' => $model->getKey(),
                'type' => get_class($model),
                'entity' => $model->toArray(),
            ];
        }

        return [
            'id' => null,
            'type' => null,
            'entity' => null,
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->tableComponent(EntityPicker::class);

        $this->default([
            'id' => null,
            'type' => null,
            'entity' => null,
        ]);

        $this->loadStateFromRelationshipsUsing(static function ($component, $record): void {
            $relationship = $component->getRelationship();
            $typeColumn = $relationship->getMorphType();
            $keyColumn = $relationship->getForeignKeyName();
            $component->state([
                'type' => $record->{$typeColumn},
                'id' => $record->{$keyColumn},
                'entity' => $component->getRelatedModel($record->{$typeColumn}, $record->{$keyColumn})?->toArray(),
            ]);
        });

        $this->saveRelationshipsUsing(static function ($component, $state, $record, $get) {
            $relationship = $component->getRelationship();
            $typeColumn = $relationship->getMorphType();
            $keyColumn = $relationship->getForeignKeyName();

            $record->update([
                $typeColumn => $state['type'],
                $keyColumn => $state['id'],
            ]);

            return $state;
        });

        $this->registerListeners([
            'entity::attach' => [
                function ($component, string $statePath, $entity): void {
                    if ($component->isDisabled()) {
                        return;
                    }

                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    foreach ($entity as $en) {
                        $this->attachEntity($en);
                    }
                },
            ],
            'entity::detach' => [
                function ($component, string $statePath): void {
                    if ($component->isDisabled()) {
                        return;
                    }

                    if ($statePath !== $component->getStatePath()) {
                        return;
                    }
                    $this->state($this->defaultState(null));
                },
            ],
        ]);
    }

    protected function attachEntity($entity)
    {
        // $set = $this->getSetCallback();
        $relationship = $this->getRelationship();
        // $typeColumn = $relationship->getMorphType();
        // $keyColumn = $relationship->getForeignKeyName();
        //dump($entity);
        $model = $this->getRelatedModel($entity['type'], $entity['id']);

        $this->state(array_merge($entity, ['entity' => $model->toArray()]));
        /*   $set($typeColumn, $entity['type']);
         $set($keyColumn, $entity['id']);*/
    }
}
