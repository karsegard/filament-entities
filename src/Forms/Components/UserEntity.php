<?php

namespace KDA\Filament\Entities\Forms\Components;

class UserEntity extends Entity
{
    protected string $view = 'filament-entities::entities.user';
}
