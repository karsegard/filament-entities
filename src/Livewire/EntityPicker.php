<?php

namespace KDA\Filament\Entities\Livewire;

use Closure;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\Layout;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Contracts\Database\Eloquent\Builder;
use KDA\Laravel\Entity\Collection\Models\Entity;
use KDA\Laravel\Entity\Collection\Models\EntityType;
use Livewire\Component;

class EntityPicker extends Component implements Tables\Contracts\HasTable
{
    use Tables\Concerns\InteractsWithTable;

    public $modalId;

    public $selecteds = [];

    public $statePath = null;

    public $isMultiple = false;

    public $type = null;

    protected $listeners = ['refreshComponent' => '$refresh'];

    protected function getTableQuery(): Builder
    {
        return Entity::query()->when(! empty($this->type), function ($query) {
            return $query->whereHas('type', function ($q) {
                return $q->where('name', $this->type);
            });
        });
    }

    protected function getTableColumns(): array
    {
        return [
            TextColumn::make('name')
                ->searchable()
                ->action(function ($record): void {
                    /*$this->dispatchBrowserEvent('open-post-edit-modal', [
                    'post' => $record->getKey(),
                ]);*/
                    // dump('hello world');
                    $selected = isset($this->selecteds[$record->getKey()]);

                    if ($selected) {
                        unset($this->selecteds[$record->getKey()]);
                    } else {
                        $this->selecteds[$record->getKey()] = [
                            'type' => $record->model_type,
                            'id' => $record->model_id,
                        ];
                    }

                    if (! $this->isMultiple) {
                        $this->dispatchBrowserEvent('close-modal', [
                            'id' => 'entity-picker',
                            'entities' => $this->selecteds,
                            'statePath' => $this->statePath, ]);
                    }
                }),
        ];
    }

    protected function getTableRecordClassesUsing(): ?Closure
    {
        return fn ($record) => match (isset($this->selecteds[$record->getKey()])) {
            true => 'bg-gray-500',
            default => null,
        };
    }

    protected function getTableFilters(): array
    {
        $options = EntityType::where('name', $this->type)->first()?->modelClasses->pluck('name', 'class')->toArray();

        return [
            // ...
            SelectFilter::make('model_type')->visible(fn () => $this->type !== null && null !== $options && count($options) > 1)->options(function ($livewire) {
                $options = EntityType::where('name', $livewire->type)->first()?->modelClasses->pluck('name', 'class')->toArray();

                if (blank($options)) {
                    $options = [];
                }

                return $options;
            }),
        ];
    }

     /*
protected function getTableFiltersLayout(): ?string
{
  //  return Layout::BelowContent;
}*/
    public function save()
    {
        $this->dispatchBrowserEvent('close-modal', [
            'id' => $this->modalId,
            'entities' => $this->selecteds,
            'statePath' => $this->statePath, ]);
    }

    public function render()
    {
        return view('filament-entities::livewire.entity-picker');
    }

    public function clearSelected()
    {
        $this->selecteds = [];
        $this->selected = null;
        $this->emit('refreshComponent');
        $this->resetTableFiltersForm();
        $this->setPage(1);
    }
}
