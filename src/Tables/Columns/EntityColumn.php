<?php

namespace KDA\Filament\Entities\Tables\Columns;

use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use KDA\Filament\Entities\Concerns\hasEntityType;
use KDA\Laravel\Entity\Collection\Models\Entity;

class EntityColumn extends TextColumn
{
    use hasEntityType;
    protected $entityRecord;
    public function setUp(): void
    {
        parent::setUp();
        // $this->formatStateUsing(fn (string $state): string => \Arr::get($state,$this->getName().'Entity'.'.name'));
        $this->getStateUsing(function ($record) {
            //dump($record->billableEntity);
            $record = $this->getRecord();
            $entity = Entity::where('model_id', $record->{$this->getName() . "_id"})->where('model_type', $record->{$this->getName() . "_type"})->first();
            return $entity->name;
        });
    }

    public function getRecord(): ?Model
    {

        return $this->record ?? $this->getLayout()?->getRecord();
    }

    public function applyEagerLoading(Builder $query): Builder
    {
        /*$model = get_class($query->getModel());
        $model::resolveRelationUsing($this->getName().'Entity', function (Model $model) {
            //return $orderModel->belongsTo(Customer::class, 'customer_id');
            return $model->belongsTo(Entity::class,$this->getName()."_type",'model_type');
        });*/

        $query = parent::applyEagerLoading($query);

        /* if ($this->queriesRelationships($query->getModel())) {
            $query->with([$this->getRelationshipName()]);
        }*/
        //return $query->with([$this->getName().'Entity']);
        return $query;
    }
}
